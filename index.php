<?php
    $gender_list = ["Nam", "Nữ"];
    $faculty_list = array(""=>"trống", "MAT"=>"Khoa học máy tính", "KDL"=>"Khoa học vật liệu");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>
<body>
    <div class="wrapper">
        <form action="#">
            <div class="input-box name-box">
                <label for="" class="label-input">Họ và tên</label>
                <input type="text" class="text-field" name="userName">
            </div>
            <div class="input-box gender-box">
                <label for="" class="label-input">Giới tính</label>
                <?php for ($i = 0; $i < count($gender_list); $i++) { ?>
                    <input type="radio" name="gender" class="radio-field" value=<?php echo $i; ?>>
                    <label for=""><?php echo $gender_list[$i]; ?></label>
                <?php } ?>
            </div>
            <div class="input-box faculty-box">
                <label for="faculty" class="label-input">Phân khoa</label>
                <select name="faculty" id="faculty" class="select-field">
                    <?php foreach($faculty_list as $key => $value) { ?>
                        <option value=<?php echo $key ?>><?php echo $value ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="button-box">
                <button type="submit">Đăng ký</button>
            </div>
        </form>
    </div>  
</body>
</html>